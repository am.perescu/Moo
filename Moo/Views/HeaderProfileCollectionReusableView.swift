//
//  HeaderProfileCollectionReusableView.swift
//  Moo
//
//  Created by Andrei Merfu on 09/05/2018.
//  Copyright © 2018 Andrei Merfu. All rights reserved.
//


import UIKit
import ProgressHUD
import Pastel


class HeaderProfileCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var postsCount: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var bio: UILabel!
        
    
    var user: User? {
        didSet {
            updateView()
            ProgressHUD.dismiss()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }
    
    func updateView() {
        self.nameLabel.text = user?.username
        self.bio.text = user?.bio
        
        if let photoUrlString = user!.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            if photoUrlString != "" {
                self.photo.sd_setImage(with: photoUrl)
            }
        }
        
        self.photo.layer.cornerRadius = self.photo.frame.size.width / 2;
        self.photo.clipsToBounds = true;
        
        API_Manager.MyPosts.fetchCountMyPosts(userId: user!.id!) {(count) in
            self.postsCount.text = "\(count)"
        }
        
        API_Manager.Follow.fetchCountFollowrs(userId: user!.id!) {(count) in
            self.followersCount.text = "\(count)"
        }
        
        API_Manager.Follow.fetchCountFollowing(userId: user!.id!) {(count) in
            self.followingCount.text = "\(count)"
        }
    }
    
    func clear() {
        self.nameLabel.text = ""
        self.bio.text = ""
        self.postsCount.text = "0"
        self.followersCount.text = "0"
        self.followingCount.text = "0"
        

        ProgressHUD.show("Waiting..", interaction: false)
    }
}

