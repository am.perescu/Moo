//
//  HomeTableViewCell.swift
//  Moo
//
//  Created by Andrei Merfu on 13/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import AVFoundation
import ProgressHUD

protocol HomeTableViewCellDelegate {
    func goToSharePostVC(image: UIImage)
}

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var likeCountButton: UIButton!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var heightConstraintPhoto: NSLayoutConstraint!
    @IBOutlet weak var volumeView: UIView!
    @IBOutlet weak var volumeButton: UIButton!

    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    var user: User? {
        didSet {
            setupUserInfo()
        }
    }
    
    var delegate: HomeTableViewCellDelegate?
    
    func updateView() {
        captionLabel.text = post?.caption
        if let ratio = post?.ratio {
            heightConstraintPhoto.constant = UIScreen.main.bounds.width / ratio
            layoutIfNeeded()
        }
        
        if let photoUrlString = post?.photoUrl {
            let photoUrl = URL(string: photoUrlString)
            if photoUrlString != "" {
                self.postImageView.sd_setImage(with: photoUrl)
            }
        }
        
        self.updateLike(post: self.post!)
        self.updateDislike(post: self.post!)
    }
    
    func setupUserInfo() {
        nameLabel.text = user?.username
        if let photoUrlString = user?.profileImageUrl {
            let photoUrl = URL(string: photoUrlString)
            if photoUrlString != "" {
                self.profileImageView.sd_setImage(with: photoUrl)
            } else {
                self.profileImageView.image = UIImage(named: "user1")
            }
        }
    }
    
    func updateLike(post: Post)
    {
        let imageName = post.likes == nil || (post.isLiked != nil && !post.isLiked!) || post.isDisliked == true ? "like" : "likeSelected"
        likeImageView.image = UIImage(named: imageName)
        let count = post.likeCount! + post.dislikeCount!
        if count != 0 {
            let likeCount = post.likeCount!
            let dislikeCount = post.dislikeCount!
            likeCountButton.setTitle("\(likeCount) likes and \(dislikeCount) dislikes", for: UIControlState.normal)
        } else {
            likeCountButton.setTitle("Be the first like or dislike this", for: UIControlState.normal)
        }
    }
    
    func updateDislike(post: Post)
    {
        let imageName = post.dislikes == nil || (post.isDisliked != nil && !post.isDisliked!) || post.isLiked == true ? "unlike" : "unlikeSelected"
        commentImageView.image = UIImage(named: imageName)
        let count = post.likeCount! + post.dislikeCount!
        if count != 0 {
            let likeCount = post.likeCount!
            let dislikeCount = post.dislikeCount!
            likeCountButton.setTitle("\(likeCount) likes and \(dislikeCount) dislikes", for: UIControlState.normal)
        } else {
            likeCountButton.setTitle("Be the first like or dislike this", for: UIControlState.normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.text = ""
        captionLabel.text = ""
        
        let tapGestureForLikeImageView = UITapGestureRecognizer(target: self, action: #selector(self.likeImageView_TouchUpInside))
        likeImageView.addGestureRecognizer(tapGestureForLikeImageView)
        likeImageView.isUserInteractionEnabled = true
        
        let tapGestureForDislikeImageView = UITapGestureRecognizer(target: self, action: #selector(self.dislikeImageView_TouchUpInside))
        commentImageView.addGestureRecognizer(tapGestureForDislikeImageView)
        commentImageView.isUserInteractionEnabled = true
        
        let shareGestureForImageView = UITapGestureRecognizer(target: self, action: #selector(self.shareImageView_TouchUpInside))
        shareImageView.addGestureRecognizer(shareGestureForImageView)
        shareImageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.postImageView_TouchUpInside))
        tap.numberOfTapsRequired = 2
        postImageView.addGestureRecognizer(tap)
        postImageView.isUserInteractionEnabled = true
        
    }
    
    @objc func postImageView_TouchUpInside() {
        likeImageView_TouchUpInside()
    }
    
    @objc func likeImageView_TouchUpInside() {
        if (self.post?.dislikeCount != 0 && ((self.post?.isDisliked == nil && self.post?.isLiked == false) || (self.post?.isDisliked == true)) ) {
            dislikeImageView_TouchUpInside()
        }
        API_Manager.Post.incrementLikes(postId: post!.id!, onSucess: {(post) in
            self.updateLike(post: post)
            self.post?.likes = post.likes
            self.post?.isLiked = post.isLiked
            self.post?.likeCount = post.likeCount
        }) {(errorMessage) in
            ProgressHUD.showError(errorMessage)
        }
    }
    
    @objc func dislikeImageView_TouchUpInside() {
        if (self.post?.likeCount != 0 && (self.post?.isLiked)!) {
            likeImageView_TouchUpInside()
        }
        API_Manager.Post.incrementDislikes(postId: post!.id!, onSuccess: {(post) in
            self.updateDislike(post: post)
            self.post?.dislikes = post.dislikes
            self.post?.isDisliked = post.isDisliked
            self.post?.dislikeCount = post.dislikeCount
        }) {(errorMessage) in
            ProgressHUD.showError(errorMessage)
        }
    }
    
    @objc func shareImageView_TouchUpInside() {
        delegate?.goToSharePostVC(image: (postImageView?.image)!)
    }
}
