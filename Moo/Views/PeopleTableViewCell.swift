//
//  PeopleTableViewCell.swift
//  Moo
//
//  Created by Antonela Madalina on 20/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
   
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var user : User? {
        didSet {
            updateView()
        }
    }
    
    func updateView(){
        name.text = user?.username
        if let photoUrlString = user?.profileImageUrl {
            if photoUrlString != "" {
                let photoUrl  = URL(string:photoUrlString)
                profileImage.sd_setImage(with: photoUrl, placeholderImage: UIImage(named:"placeholderImg"))
            } else {
                profileImage.image = UIImage(named: "user1")
            }
        }

        if user!.isFollowing! {
            configureUnFollowButton()
        } else {
            configureFollowButton()
        }
    
}

    func configureFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232.255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        
        followButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        followButton.backgroundColor = UIColor(red: 69/255, green: 142/255, blue: 255/255, alpha: 1)
        followButton.setTitle("Follow", for: UIControlState.normal)
        followButton.addTarget(self, action: #selector(self.followAction), for: UIControlEvents.touchUpInside)
    }

    func configureUnFollowButton() {
        followButton.layer.borderWidth = 1
        followButton.layer.borderColor = UIColor(red: 226/255, green: 228/255, blue: 232.255, alpha: 1).cgColor
        followButton.layer.cornerRadius = 5
        followButton.clipsToBounds = true
        
        followButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        followButton.backgroundColor = UIColor.clear
        followButton.setTitle("Following", for: UIControlState.normal)
        followButton.addTarget(self, action: #selector(self.unFollowAction), for: UIControlEvents.touchUpInside)
    }

    @objc func followAction() {
        if user!.isFollowing! == false {
            API_Manager.Follow.followAction(withUser: user!.id!)
            configureUnFollowButton()
            user!.isFollowing! = true
        }
    }

    @objc func unFollowAction() {
        if user!.isFollowing! == true {
            API_Manager.Follow.unfollowAction(withUser: user!.id!)
            configureFollowButton()
            user!.isFollowing! = false
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
