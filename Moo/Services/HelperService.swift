//
//  HelperService.swift
//  Moo
//
//  Created by Andrei Merfu on 13/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseStorage
import FirebaseAuth
import ProgressHUD

class HelperService {
    static func uploadDataToServer(data: Data, ratio: CGFloat, caption: String, onSuccess: @escaping() -> Void) {
            uploadImageToFirebaseStorage(data: data) {(photoUrl) in
            self.sendDataToDatabase(photoUrl: photoUrl, ratio: ratio, caption: caption, onSuccess: onSuccess)
        }
    }
    
    static func uploadImageToFirebaseStorage(data: Data, onSuccess: @escaping (_ imageUrl: String) -> Void) {
        let photoIdString = NSUUID().uuidString
        let storageRef = Storage.storage().reference(forURL: Config.STORAGE_ROOF_REF).child("posts").child(photoIdString)
        storageRef.putData(data, metadata: nil) { (metadata, error) in
            if error != nil {
                ProgressHUD.showError(error!.localizedDescription)
                return
            }
            if let photoUrl = metadata?.downloadURL()?.absoluteString {
                onSuccess(photoUrl)
            }
        }
    }
    
    static func sendDataToDatabase(photoUrl: String, ratio: CGFloat, caption: String, onSuccess: @escaping() -> Void) {
        let newPostId = API_Manager.Post.REF_POSTS.childByAutoId().key
        let newPostReference = API_Manager.Post.REF_POSTS.child(newPostId)
        
        let currentUser = Auth.auth().currentUser
        
        let currentUserId = currentUser?.uid
        let dict = ["uid": currentUserId as Any, "photoUrl": photoUrl, "caption": caption, "likeCount": 0, "dislikeCount": 0,"ratio": ratio] as [String: Any]
        
        newPostReference.setValue(dict, withCompletionBlock: {
            (error, ref) in
            if error != nil {
                ProgressHUD.showError(error!.localizedDescription)
                return
            }
            API_Manager.Feed.REF_FEED.child(API_Manager.User.CURRENT_USER_UID!).child(newPostId).setValue(true)
            let myPostsRef = API_Manager.MyPosts.REF_MYPOSTS.child(currentUserId!).child(newPostId)
            myPostsRef.setValue(true, withCompletionBlock: { (error, ref) in
                if error != nil {
                    ProgressHUD.showError(error!.localizedDescription)
                    return
                }
            })
            ProgressHUD.showSuccess("Success")
            onSuccess()
        })
    }
    
}
