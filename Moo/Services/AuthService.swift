//
//  AuthService.swift
//  Moo
//
//  Created by Andrei Merfu on 12/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class AuthService {
    
    static func signUp(username: String, email: String, password: String, onSuccess: @escaping () -> Void, onError: @escaping(_ errorMessage: String?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            let uid = user?.uid
            self.setUserInformation(profileImageUrl: "", bio: "", username: username, email: email, uid: uid!, onSuccess: onSuccess)
        })
    }
    
    static func setUserInformation(profileImageUrl: String, bio: String, username: String, email: String, uid: String, onSuccess: @escaping () -> Void) {
        let ref = Database.database().reference()
        let usersReference = ref.child("users")
        let newUserReference = usersReference.child(uid)
        newUserReference.setValue(["username": username, "username_lowercase": username.lowercased(), "email": email, "profileImageUrl": profileImageUrl, "bio": bio])
        onSuccess()
    }
    
    static func signIn(email: String, password: String, onSuccess: @escaping () -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error != nil {
                onError(error!.localizedDescription)
                return
            }
            onSuccess()
        })
    }
    
    static func logout(onSuccess: @escaping () -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        do {
            try Auth.auth().signOut()
            onSuccess()
        } catch let logoutError {
            onError(logoutError.localizedDescription)
        }
    }
    
    static func updateUserInfo(username: String,bio:String,imageData: Data , onSuccess: @escaping () -> Void ,onError: @escaping(_ errorMessage: String?) -> Void) {
      
                let uid = API_Manager.User.CURRENT_USER_UID!
                let storageRef = Storage.storage().reference(forURL: Config.STORAGE_ROOF_REF).child("profile_image").child(uid)
                
                storageRef.putData(imageData, metadata: nil, completion: { (metadata, error) in
                    if error != nil {
                        return
                    }
                    let profileImageUrl = metadata?.downloadURL()?.absoluteString
                    
                    self.updateDatabase(profileImageUrl: profileImageUrl!, username: username, bio: bio, onSuccess: onSuccess, onError: onError)
                })
            }
    
    
    static func updateDatabase(profileImageUrl: String, username: String, bio: String, onSuccess: @escaping () -> Void, onError:  @escaping (_ errorMessage: String?) -> Void) {
        let dict = ["username": username,"username_lowercase":username.lowercased(),"bio":bio,"profileImageUrl":profileImageUrl]
        API_Manager.User.REF_CURRENT_USER?.updateChildValues(dict , withCompletionBlock: { (error,ref) in
            if(error != nil) {
                onError(error!.localizedDescription)
            } else{
                onSuccess()
            }
        })
    }
}
