//
//  MyPosts_API.swift
//  Moo
//
//  Created by Andrei Merfu on 13/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseDatabase

class MyPostsApi {
    var REF_MYPOSTS = Database.database().reference().child("myPosts")
    
    func fetchMyPosts(userId: String, completion: @escaping (String) -> Void) {
        REF_MYPOSTS.child(userId).observe(.childAdded, with: {
            snapshot in
            completion(snapshot.key)
        })
    }
    
    func fetchCountMyPosts(userId: String, completion: @escaping(Int) -> Void) {
        REF_MYPOSTS.child(userId).observe(.value, with: {
            snapshot in
            let count = Int(snapshot.childrenCount)
            completion(count)
        })
    }
    
    func deletePost(postId: String) {
        REF_MYPOSTS.child(API_Manager.User.CURRENT_USER_UID!).child(postId).removeValue(completionBlock: {(error, ref) in
            if error != nil {
                print("Failed to delete post:", error as Any)
            }
        })
        
        // Remove from posts
        API_Manager.Post.REF_POSTS.child(postId).removeValue()
        
        // Remove from feed
        API_Manager.Feed.REF_FEED.child(API_Manager.User.CURRENT_USER_UID!).child(postId).removeValue()
        
        // TODO Remove for each child 
    }
}
