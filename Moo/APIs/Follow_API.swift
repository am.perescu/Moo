//
//  Follow_API.swift
//  Moo
//
//  Created by Andrei Merfu on 20/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FollowApi {
    var REF_FOLLOWERS = Database.database().reference().child("followers")
    var REF_FOLLOWING = Database.database().reference().child("following")
    
    
    
    func followAction(withUser id: String) {
        API_Manager.MyPosts.REF_MYPOSTS.child(id).observeSingleEvent(of: .value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                for key in dict.keys {
                    Database.database().reference().child("feed").child(API_Manager.User.CURRENT_USER_UID!).child(key).setValue(true)
                }
            }
        })
        REF_FOLLOWERS.child(id).child(API_Manager.User.CURRENT_USER_UID!).setValue(true)
        REF_FOLLOWING.child(API_Manager.User.CURRENT_USER_UID!).child(id).setValue(true)
    }
    
    func unfollowAction(withUser id: String) {
        API_Manager.MyPosts.REF_MYPOSTS.child(id).observeSingleEvent(of: .value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                for key in dict.keys {
                    Database.database().reference().child("feed").child(API_Manager.User.CURRENT_USER_UID!).child(key).removeValue()
                }
            }
        })
        
        REF_FOLLOWERS.child(id).child(API_Manager.User.CURRENT_USER_UID!).setValue(NSNull())
        REF_FOLLOWING.child(API_Manager.User.CURRENT_USER_UID!).child(id).setValue(NSNull())
    }
    
    func isFollowing(userId: String, completed: @escaping (Bool) -> Void) {
        REF_FOLLOWERS.child(userId).child(API_Manager.User.CURRENT_USER_UID!).observeSingleEvent(of: .value, with: {
            snapshot in
            if let _ = snapshot.value as? NSNull {
                completed(false)
            } else {
                completed(true)
            }
        })
    }
    
    func fetchCountFollowing(userId: String, completion: @escaping (Int) -> Void) {
        REF_FOLLOWING.child(userId).observe(.value, with: {
            snapshot in
            let count = Int(snapshot.childrenCount)
            completion(count)
        })
    }
    
    func fetchCountFollowrs(userId: String, completion: @escaping(Int) -> Void) {
        REF_FOLLOWERS.child(userId).observe(.value, with: {
            snapshot in
            let count = Int(snapshot.childrenCount)
            completion(count)
        })
    }
    
    
}
