//
//  API_Manager.swift
//  Moo
//
//  Created by Andrei Merfu on 12/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation

struct API_Manager {
    static var User = UserApi()
    static var Post = PostApi()
    static var Feed = FeedApi()
    static var MyPosts = MyPostsApi()
    static var Follow = FollowApi()
}
