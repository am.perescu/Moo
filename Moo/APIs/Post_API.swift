//
//  Post_API.swift
//  Moo
//
//  Created by Andrei Merfu on 13/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseDatabase

class PostApi {
    var REF_POSTS = Database.database().reference().child("posts")
    
    func observePosts(completion: @escaping (Post) -> Void) {
        REF_POSTS.observe(.childAdded) {
            (snapshot: DataSnapshot) in
            if let dict = snapshot.value as? [String: Any] {
                let newPost = Post.transformPostPhoto(dict: dict, key: snapshot.key)
                completion(newPost)
            }
        }
    }
    
    func observePost(withId id: String, completion: @escaping (Post) -> Void) {
        REF_POSTS.child(id).observeSingleEvent(of: DataEventType.value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let post = Post.transformPostPhoto(dict: dict, key: snapshot.key)
                completion(post)
            }
        })
    }
    
    func incrementLikes(postId: String, onSucess: @escaping (Post) -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        let postRef = API_Manager.Post.REF_POSTS.child(postId)
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var post = currentData.value as? [String : AnyObject], let uid = API_Manager.User.CURRENT_USER_UID {
                var likes: Dictionary<String, Bool>
                likes = post["likes"] as? [String : Bool] ?? [:]
                var likeCount = post["likeCount"] as? Int ?? 0
                if let _ = likes[uid] {
                    likeCount -= 1
                    likes.removeValue(forKey: uid)
                } else {
                    likeCount += 1
                    likes[uid] = true
                }
                post["likeCount"] = likeCount as AnyObject?
                post["likes"] = likes as AnyObject?
                
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dict = snapshot?.value as? [String: Any] {
                let post = Post.transformPostPhoto(dict: dict, key: snapshot!.key)
                onSucess(post)
            }
        }
    }
    
    func incrementDislikes(postId: String, onSuccess: @escaping(Post) -> Void, onError: @escaping(_ errorMessage: String?) -> Void) {
        let postRef = API_Manager.Post.REF_POSTS.child(postId)
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var post = currentData.value as? [String: AnyObject], let uid = API_Manager.User.CURRENT_USER_UID {
                var dislikes: Dictionary<String, Bool>
                dislikes = post["dislikes"] as? [String : Bool] ?? [:]
                var dislikeCount = post["dislikeCount"] as? Int ?? 0
                if let _ = dislikes[uid] {
                    dislikeCount -= 1
                    dislikes.removeValue(forKey: uid)
                } else {
                    dislikeCount += 1
                    dislikes[uid] = true
                }
                post["dislikeCount"] = dislikeCount as AnyObject?
                post["dislikes"] = dislikes as AnyObject?
                
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, comitted, snapshot) in
                if let error = error {
                    onError(error.localizedDescription)
                }
                if let dict = snapshot?.value as? [String: Any] {
                    let post = Post.transformPostPhoto(dict: dict, key: snapshot!.key)
                    onSuccess(post)
                }
            }
        }
}
