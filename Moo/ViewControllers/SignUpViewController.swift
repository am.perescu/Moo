//
//  SignUpViewController.swift
//  Moo
//
//  Created by Andrei Merfu on 17/03/2018.
//  Copyright © 2018 Andrei Merfu. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import ProgressHUD
import Pastel

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frontend()
        
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.hideKeyboard(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
        
        
        
    }
    
    func frontend() {
        usernameField.backgroundColor = UIColor.clear
        usernameField.tintColor = UIColor.white
        usernameField.textColor = UIColor.white
        usernameField.attributedPlaceholder = NSAttributedString(string: usernameField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
        let bottomLayerUsername = CALayer()
        bottomLayerUsername.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
        bottomLayerUsername.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
       // usernameField.layer.addSublayer(bottomLayerUsername)
        
        emailField.backgroundColor = UIColor.clear
        emailField.tintColor = UIColor.white
        emailField.textColor = UIColor.white
        emailField.attributedPlaceholder = NSAttributedString(string: emailField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
        let bottomLayerEmail = CALayer()
        bottomLayerEmail.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
        bottomLayerEmail.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
       // emailField.layer.addSublayer(bottomLayerEmail)
        
        passwordField.backgroundColor = UIColor.clear
        passwordField.tintColor = UIColor.white
        passwordField.textColor = UIColor.white
        passwordField.attributedPlaceholder = NSAttributedString(string: passwordField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
        let bottomLayerPassword = CALayer()
        bottomLayerPassword.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
        bottomLayerPassword.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
        //passwordField.layer.addSublayer(bottomLayerPassword)
        
        
        let pastelView = PastelView(frame: view.bounds)
        
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
        
        
    }
    
    
    
    
    // hide keyboard func
    @objc func hideKeyboard(_ recognizer : UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func dismiss_onClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func signUpBtn_TouchUpInside(_ sender: Any) {
//        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!, completion: { (user: User?, error: Error?) in
//            if error != nil {
//                print(error!.localizedDescription)
//                return
//            }
//            let ref = Database.database().reference();
//            let usersReference = ref.child("users")
//            let uid = user?.uid
//            let newUserReference =  usersReference.child(uid!)
//            newUserReference.setValue(["username": self.usernameField.text!,
//                                       "email": self.emailField.text!
//                                     ])
//            self.performSegue(withIdentifier: "signUpToTabBar", sender: nil)
//
//        })
//
//    }
    
    @IBAction func signUpBtn_TouchUpInside(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting...", interaction: false)
        AuthService.signUp(username: usernameField.text!, email: emailField.text!, password: passwordField.text!, onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.performSegue(withIdentifier: "signUpToTabBar", sender: nil)
        }, onError: {(errorString) in
            ProgressHUD.showError(errorString)
        })
    }
    

}
