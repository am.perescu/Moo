//
//  SettingsTableViewController.swift
//  Moo
//
//  Created by Antonela Madalina on 26/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import ProgressHUD

protocol SettingsTableViewControllerDelegate {
    func updateUserInfo()
}

class SettingsTableViewController: UITableViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    var delegate: SettingsTableViewControllerDelegate?

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var bioTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        bioTextField.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self,action:#selector(self.handleSelectedPhoto))
        profileImageView.addGestureRecognizer(tapGesture)
        profileImageView.isUserInteractionEnabled = true
        
        fetchCurrentUser()
    }
    
    @objc func handleSelectedPhoto(){
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        present(pickerController,animated: true , completion: nil)
    }
    
    func fetchCurrentUser() {
        API_Manager.User.getCurrentUser{(user) in
            if let profileUrl = URL(string: user.profileImageUrl!) {
                self.profileImageView.sd_setImage(with: profileUrl)
            }
            self.nameTextField.text = user.username
            self.bioTextField.text = user.bio
            
        }
    }
    
    @IBAction func saveBtn_TouchUpInside(_ sender: Any) {
        if let profileImg = self.profileImageView.image , let imageData = UIImageJPEGRepresentation(profileImg, 0.1){
            ProgressHUD.show("Waiting...")
            AuthService.updateUserInfo(username: nameTextField.text!, bio: bioTextField.text!, imageData: imageData, onSuccess: {
                ProgressHUD.showSuccess("Success")
                self.delegate?.updateUserInfo()
                self.navigationController?.popViewController(animated: true)
            }, onError: { (errorMessage) in
                ProgressHUD.showError(errorMessage)
                })
        }
    }
    
    @IBAction func logout_Btn_TouchUpInside(_ sender: Any) {
        AuthService.logout(onSuccess: {
            let storyboard = UIStoryboard(name: "Start", bundle: nil)
            let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
            self.present(signInVC, animated: true, completion: nil)
        }, onError: {(errorMessage) in
            ProgressHUD.showError(errorMessage)
        })
    }
    func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info:[String : Any]){
        print("did Finish Picking Media")
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
          //  selectedImage = image
            profileImageView.image = image
            
            dismiss(animated: true, completion: nil)
        }
    }
    
}

extension SettingsTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return")
        textField.resignFirstResponder()
        return true
    }
}
