//
//  PhotoViewController.swift
//  Moo
//
//  Created by Andrei Merfu on 19/03/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import ProgressHUD

class PhotoViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    
    var selectedImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self,action:#selector(self.handleSelectedPhoto))
        photo.addGestureRecognizer(tapGesture)
        photo.isUserInteractionEnabled = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handlePost()
    }
    
    func handlePost() {
        if selectedImage != nil {
            self.shareButton.isEnabled = true
            self.removeButton.isEnabled = true
            self.shareButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
            self.shareButton.isEnabled = false
            self.removeButton.isEnabled = false
            self.shareButton.backgroundColor = .lightGray
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
  
    @objc func handleSelectedPhoto(){
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        present(pickerController,animated: true , completion: nil)
    }
 
    @IBAction func remove_TouchUpInside(_ sender: Any) {
        clean()
        handlePost()
    }
    
//    @IBAction func shareButton_TouchUpInside(_ sender: Any) {
//        view.endEditing(true)
//        if let pickedImg = self.selectedImage , let imageData = UIImageJPEGRepresentation(pickedImg, 0.1){
//        let photoId = NSUUID().uuidString
//            let storageRef = Storage.storage().reference(forURL:"gs://dbmoo-553bf.appspot.com/").child("posts").child(photoId)
//            storageRef.putData(imageData, metadata: nil,completion: { (metadata,error) in
//                if error != nil{
//                    return
//                }
//                let photoURL = metadata?.downloadURL()?.absoluteString
//                self.sendDataToDatabase(photoURL : photoURL!)
//                })
//        }
//    }
    
    @IBAction func shareButton_TouchUpInside(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting...", interaction: false)
        if let pickedImg = self.selectedImage, let imageData = UIImageJPEGRepresentation(pickedImg, 0.1) {
            let ratio = pickedImg.size.width / pickedImg.size.height
            HelperService.uploadDataToServer(data: imageData, ratio: ratio, caption: captionTextView.text!, onSuccess: {
                self.clean()
                self.tabBarController?.selectedIndex = 0
            } )
        } else {
            ProgressHUD.showError("Image can't be empty")
        }
    }
    
    
    
    @IBAction func openCameraButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert:UIAlertController = UIAlertController(title: "Camera Unavailable", message: "Unable to find a camera on this device", preferredStyle: UIAlertControllerStyle.alert)
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    
    func sendDataToDatabase(photoURL: String){
        ProgressHUD.show("Waiting...", interaction: false)
        let ref = Database.database().reference();
        let postsReference = ref.child("posts")
        let newPostId = postsReference.childByAutoId().key
        let newPostReference = postsReference.child(newPostId)
        newPostReference.setValue(["photoURL": photoURL, "caption": captionTextView.text!],withCompletionBlock: {
            (error,ref) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }
            ProgressHUD.showSuccess("Success")
            self.captionTextView.text = ""
            self.photo.image = UIImage(named: "placeholder-photo")
            self.selectedImage = nil
            self.tabBarController?.selectedIndex = 0
    
        })

    }
    
    func clean() {
        self.captionTextView.text = ""
        self.photo.image = UIImage(named: "placeholder-photo")
        self.selectedImage = nil
    }
    
    func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info:[String : Any]){
        print("did Finish Picking Media")
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImage = image
            photo.image = image
       
        dismiss(animated: true, completion: nil)
        }
    }

}

