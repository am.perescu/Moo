//
//  HomeViewController.swift
//  Moo
//
//  Created by Andrei Merfu on 19/03/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import FirebaseAuth
import SDWebImage

class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    private let refreshControl = UIRefreshControl()
    
    var users = [User]()
    var posts = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 521
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.dataSource = self
        //loadPosts()
        self.refreshTableView()


        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(HomeViewController.refreshTableView), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        
    }
 
    func loadPosts() {
        
        API_Manager.Feed.observeFeed(withId: API_Manager.User.CURRENT_USER_UID!) { (post) in
            guard let postUid = post.uid else {
                return
            }
            self.fetchUser(uid: postUid, completed: {
                self.posts.insert(post, at: 0)
                self.tableView.reloadData()
            })
        }
        
        API_Manager.Feed.observeFeedRemoved(withId: API_Manager.User.CURRENT_USER_UID!) { (post) in
            self.posts = self.posts.filter { $0.id != post.id }
            self.users = self.users.filter { $0.id != post.uid }
            self.tableView.reloadData()
        }
    }
    
    @objc func refreshTableView() {
        self.posts.removeAll()
        self.users.removeAll()
        tableView.reloadData()
        self.loadPosts()
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func fetchUser(uid: String, completed: @escaping() -> Void) {
        API_Manager.User.observeUser(withId: uid, completion: { user in
            self.users.insert(user, at: 0)
            completed()
        })
    }
}


extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! HomeTableViewCell
        let post = posts[indexPath.row]
        let user = users[indexPath.row]
        cell.post = post
        cell.user = user
        cell.delegate = self
        return cell
    }
}

extension HomeViewController: HomeTableViewCellDelegate {
    func goToSharePostVC(image: UIImage) {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            vc.popoverPresentationController?.sourceView = self.view
        
        
        vc.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
            present(vc, animated: true, completion: nil)
    }
}
