//
//  PeopleViewController.swift
//  Moo
//
//  Created by Antonela Madalina on 20/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var users  : [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        loadUsers()
    }

    func loadUsers() {
        API_Manager.User.observeUsers {(user) in
            if (user.id != API_Manager.User.CURRENT_USER_UID!) {
                self.isFollowing(userId: user.id!,completed: {(value) in
                    user.isFollowing = value
                    self.users.append(user)
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    func isFollowing(userId: String ,completed: @escaping (Bool) -> Void){
        API_Manager.Follow.isFollowing(userId: userId, completed: completed)
    }
}
extension PeopleViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleTableViewCell", for: indexPath) as! PeopleTableViewCell
        let user = users[indexPath.row]
        cell.user = user
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let user = self.users[indexPath.row]
        if user.isFollowing == false {
            let follow = UIContextualAction(style: .normal, title: "Follow") { (action, view, handler) in
                let user = self.users[indexPath.row]
                API_Manager.Follow.followAction(withUser: user.id!)
                self.users.removeAll()
                self.tableView.reloadData()
                self.loadUsers()
                handler(true)
                self.tableView.reloadData()
            }
            follow.backgroundColor = .green
            return UISwipeActionsConfiguration(actions: [follow])
        } else {
            let unfollow = UIContextualAction(style: .normal, title: "Unfollow") { (action, view, handler) in
                let user = self.users[indexPath.row]
                API_Manager.Follow.unfollowAction(withUser: user.id!)
                self.users.removeAll()
                self.tableView.reloadData()
                self.loadUsers()
                handler(true)
                self.tableView.reloadData()
            }
            unfollow.backgroundColor = .orange
            let configuration =  UISwipeActionsConfiguration(actions: [unfollow])
            return configuration
        }
       
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Nope") {(action, view, handler) in



            self.users.remove(at: indexPath.row)
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
            handler(true)
        }
        let configuration =  UISwipeActionsConfiguration(actions: [delete])
        return configuration
    }

    
}

extension PeopleViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 60
//    }
}
