//
//  ProfileViewController.swift
//  Moo
//
//  Created by Andrei Merfu on 19/03/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import ProgressHUD
import FirebaseAuth
import SDWebImage
import Pastel
import FirebaseStorage

class ProfileViewController: UIViewController, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var user: User!
    var posts: [Post] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true;
        collectionView.dataSource = self
        getUserInfo()
        fetchMyPosts()
        
        // long press gesture
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func fetchUser() {
        API_Manager.User.getCurrentUser {(user) in
            self.user = user
        }
    }
    
    func getUserInfo() {
        API_Manager.User.getCurrentUser {(user) in
            self.user = user
            self.navigationItem.title = user.username
            self.collectionView.reloadData()
        }
    }
    
    func fetchMyPosts() {
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        API_Manager.MyPosts.REF_MYPOSTS.child(currentUser.uid).observe(.childAdded, with: {
            snapshot in
            API_Manager.Post.observePost(withId: snapshot.key, completion: {post in
                self.posts.insert(post, at:0)
                self.collectionView.reloadData()
            })
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Profile_SettingsSegue" {
            let settingsVC = segue.destination as! SettingsTableViewController
            settingsVC.delegate = self
        }
    }
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        
        if (gestureRecognizer.state != UIGestureRecognizerState.ended){
            return
        }
        
        let p = gestureRecognizer.location(in: self.collectionView)
        
        if let indexPath = self.collectionView?.indexPathForItem(at: p)! {
            //do whatever you need to do
            
            // Add blur effect when UIAlert is visible
            let blurEffect = UIBlurEffect(style: .light)
            let blurVisualEffectView = UIVisualEffectView(effect: blurEffect)
            blurVisualEffectView.frame = view.bounds
            
            
            
            let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to delete this picture?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                blurVisualEffectView.removeFromSuperview()
                let post = self.posts[indexPath.item]
                API_Manager.MyPosts.deletePost(postId: post.id!)
                
                let storage = Storage.storage()
                let url = self.posts[indexPath.item].photoUrl
                let storageRef = storage.reference(forURL: url!)
                
                storageRef.delete { error in
                    if let error = error {
                        print(error)
                    } else {
                        self.posts.removeAll()
                        self.collectionView.reloadData()
                        self.fetchMyPosts()
                    }
                }
                
                
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) -> Void in
                blurVisualEffectView.removeFromSuperview()
            })
            
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            self.view.addSubview(blurVisualEffectView)
            self.present(dialogMessage, animated: true, completion: nil)
        }
        
    }
}

extension ProfileViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        let post = posts[indexPath.row]
        cell.post = post
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerViewCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderProfileCollectionReusableView", for: indexPath) as! HeaderProfileCollectionReusableView
        
        if let userCurrent = self.user {
            headerViewCell.user = userCurrent
        }
        
        let pastelView = PastelView(frame: headerViewCell.bounds)
        
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        headerViewCell.insertSubview(pastelView, at: 0)
        
        return headerViewCell
    }
}

extension ProfileViewController: SettingsTableViewControllerDelegate {
    func updateUserInfo() {
        self.getUserInfo()
    }
}

