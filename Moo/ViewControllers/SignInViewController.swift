//
//  SignInViewController.swift
//  Moo
//
//  Created by Antonela Madalina on 24/03/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import UIKit
import FirebaseAuth
import ProgressHUD
import Pastel

class SignInViewController: UIViewController{

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        frontend()
    
        /* Auto-login*/
        if Auth.auth().currentUser != nil {
            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false, block: {(timer) in
                self.performSegue(withIdentifier: "signInToTabBar", sender: nil)

            })
        }
        // tap to hide keyboard
        let hideTap = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.hideKeyboard(_:)))
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(hideTap)
        /**/
        
    }
    
    func frontend() {
        /**
         * Frontend
         */
        // ====================================================================================
        emailField.backgroundColor = UIColor.clear
        emailField.tintColor = UIColor.white
        emailField.textColor = UIColor.white
        emailField.attributedPlaceholder = NSAttributedString(string: emailField.placeholder!, attributes:
            [NSAttributedStringKey.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
        let bottomLayerEmail = CALayer()
        bottomLayerEmail.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
        bottomLayerEmail.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
       // emailField.layer.addSublayer(bottomLayerEmail)
        
        passwordField.backgroundColor = UIColor.clear
        passwordField.tintColor = UIColor.white
        passwordField.textColor = UIColor.white
        passwordField.attributedPlaceholder = NSAttributedString(string: passwordField.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
        let bottomLayerPassword = CALayer()
        bottomLayerPassword.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
        bottomLayerPassword.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
        //passwordField.layer.addSublayer(bottomLayerPassword)
        // ====================================================================================
        
        let pastelView = PastelView(frame: view.bounds)
        
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
        

    
    // hide keyboard func
    @objc func hideKeyboard(_ recognizer : UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
//    @IBAction func signInBtn_TouchUpInside(_ sender: Any) {
//        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!, completion: {(user,error) in
//            if error != nil {
//                print(error!.localizedDescription)
//                return
//            }
//
//            self.performSegue(withIdentifier: "signInToTabBar", sender: nil)
//
//        })
//    }
    
    @IBAction func signInBtn_TouchUpInside(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting...", interaction: false)
        AuthService.signIn(email: emailField.text!, password: passwordField.text!, onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.performSegue(withIdentifier: "signInToTabBar", sender: nil)
        }, onError: { error in
            ProgressHUD.showError(error!)
        })
    }
    
}
