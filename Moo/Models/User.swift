//
//  User.swift
//  Moo
//
//  Created by Andrei Merfu on 12/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation

class User {
    var email: String?
    var profileImageUrl: String?
    var username: String?
    var id: String?
    var isFollowing: Bool?
    var bio: String?
}

extension User {
    static func transformUser(dict: [String: Any], key: String) -> User {
        let user = User()
        user.email = dict["email"] as? String
        user.profileImageUrl = dict["profileImageUrl"] as? String
        user.username = dict["username"] as? String
        user.bio = dict["bio"] as? String
        user.id = key
        return user
    }
}
