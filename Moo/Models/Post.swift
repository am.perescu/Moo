//
//  Post.swift
//  Moo
//
//  Created by Andrei Merfu on 13/05/2018.
//  Copyright © 2018 Antonela Madalina. All rights reserved.
//

import Foundation
import FirebaseAuth

class Post {
    var caption: String?
    var photoUrl: String?
    var uid: String?
    var id: String?
    var likeCount: Int?
    var dislikeCount: Int?
    var likes: Dictionary<String, Any>?
    var dislikes: Dictionary<String, Any>?
    var isLiked: Bool?
    var isDisliked: Bool?
    var ratio: CGFloat?
}

extension Post {
    static func transformPostPhoto(dict: [String: Any], key: String) -> Post {
        let post = Post()
        post.id = key
        post.caption = dict["caption"] as? String
        post.photoUrl = dict["photoUrl"] as? String
        post.uid = dict["uid"] as? String
        post.likeCount = dict["likeCount"] as? Int
        post.dislikeCount = dict["dislikeCount"] as? Int
        post.likes = dict["likes"] as? Dictionary<String, Any>
        post.dislikes = dict["dislikes"] as? Dictionary<String, Any>
        post.ratio = dict["ratio"] as? CGFloat
        if let currentUserId = Auth.auth().currentUser?.uid {
            if post.likes != nil && post.isDisliked == nil {
                post.isLiked = post.likes![currentUserId] != nil
            }
            if post.dislikes != nil && post.isLiked == nil{
                post.isDisliked = post.dislikes![currentUserId] != nil
            }
        }
        return post
    }
}
